/*--------------------------------------------------------------------------------*/

####  SSH Security Group
module "ssh_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/ssh_sg"
  name            = "Stage Ssh SG"
  source_cidr     = "10.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage SSH Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Monitoring Security Group
module "monitoring_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/monitoring_sg"
  name            = "Stage Monitoring SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Monitoring Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Web Security Group
module "web_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/web_sg"
  name            = "Stage Web SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Web Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Tomcat Security Group
module "tomcat_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/tomcat_sg"
  name            = "Stage Tomcat SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Tomcat Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Mysql Security Group
module "mysql_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mysql_sg"
  name            = "Stage Mysql SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Mysql Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Mongo Security Group
module "mongo_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mongo_sg"
  name            = "Stage Mongo SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Mongo Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Mongoose Security Group
module "mongoose_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mongooseim_sg"
  name            = "Stage Mongoose SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Mongoose Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Tungsten Security Group
module "tungsten_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/tungsten_sg"
  name            = "Stage Tungsten SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage Tungsten Security Group"
}

/*--------------------------------------------------------------------------------*/

####  ELB Web Security Group
module "elbweb_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/elbweb_sg"
  name            = "Stage ELB API SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Stage ELB API Security Group"
}

/*--------------------------------------------------------------------------------*/

####  Key Pair
module "stg_key_pair" {
  source          = "github.com/sajal-git/terraform-modules//key_pair"
  public_key_path = "${var.stg_key_path}"
  keyname         = "${var.stgkeyname}"
}

/*--------------------------------------------------------------------------------*/
