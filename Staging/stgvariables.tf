/*---------------------------------*/

####  Key Pair
variable "stg_key_path" {
  default = "keys/stg-key.pub"
} 
variable "stgkeyname"{
  default = "stg-key"
} 

/*---------------------------------*/

####  Stage BL Instance
variable "stgbl_ami_id" {
  default = "ami-ce1464a1"
}
variable "stgbl_ins_type" {
  default = "t2.nano"
}
variable "stgbl_pvt_ip" {
  default = "10.0.8.11"
}
variable "stgbl_name" {
  default = "Stage_SOA_BL_API"
}
variable "stgbl_volume_size" {
  default = "30"
}
variable "stgbl_elb_name" {
  default = "stgbl"
}

/*---------------------------------*/

####  Stage BL DB Instance
variable "stgbldb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stgbldb_ins_type" {
  default = "t2.nano"
}
variable "stgbldb_pvt_ip" {
  default = "10.0.9.11"
}
variable "stgbldb_name" {
  default = "Stage_SOA_BL_DB"
}
variable "stgbldb_volume_size" {
  default = "30"
}

/*---------------------------------*/

####  Stage Admin Instance
variable "stgadmin_ami_id" {
  default = "ami-5215653d"
}
variable "stgadmin_ins_type" {
  default = "t2.nano"
}
variable "stgadmin_pvt_ip" {
  default = "10.0.8.12"
}
variable "stgadmin_name" {
  default = "Stage_SOA_Sonb_API"
}
variable "stgadmin_volume_size" {
  default = "30"
}
variable "stgadmin_elb_name" {
  default = "stgadmin"
}

/*---------------------------------*/

####  Stage Admin DB Instance
variable "stgadmindb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stgadmindb_ins_type" {
  default = "t2.nano"
}
variable "stgadmindb_pvt_ip" {
  default = "10.0.9.12"
}
variable "stgadmindb_name" {
  default = "Stage_SOA_Sonb_DB"
}
variable "stgadmindb_volume_size" {
  default = "30"
}

/*---------------------------------*/

####  Stage Assets Instance
variable "stgassets_ami_id" {
  default = "ami-4413632b"
}
variable "stgassets_ins_type" {
  default = "t2.nano"
}
variable "stgassets_pvt_ip" {
  default = "10.0.8.13"
}
variable "stgassets_name" {
  default = "Stage_SOA_Assets_API"
}
variable "stgassets_volume_size" {
  default = "30"
}
variable "stgassets_elb_name" {
  default = "stgassets"
}

/*---------------------------------*/

####  Stage Assets DB Instance
variable "stgassetsdb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stgassetsdb_ins_type" {
  default = "t2.nano"
}
variable "stgassetsdb_pvt_ip" {
  default = "10.0.9.13"
}
variable "stgassetsdb_name" {
  default = "Stage_SOA_Assets_DB"
}
variable "stgassetsdb_volume_size" {
  default = "30"
}

/*---------------------------------*/

####  Stage Content Instance
variable "stgcontent_ami_id" {
  default = "ami-5d166632"
}
variable "stgcontent_ins_type" {
  default = "t2.nano"
}
variable "stgcontent_pvt_ip" {
  default = "10.0.8.14"
}
variable "stgcontent_name" {
  default = "Stage_SOA_Content_API"
}
variable "stgcontent_volume_size" {
  default = "30"
}
variable "stgcontent_elb_name" {
  default = "stgcontent"
}

/*---------------------------------*/

####  Stage Content DB Instance
variable "stgcontentdb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stgcontentdb_ins_type" {
  default = "t2.nano"
}
variable "stgcontentdb_pvt_ip" {
  default = "10.0.9.14"
}
variable "stgcontentdb_name" {
  default = "Stage_SOA_Content_DB"
}
variable "stgcontentdb_volume_size" {
  default = "30"
}

/*---------------------------------*/

####  Stage Group Instance
variable "stggroup_ami_id" {
  default = "ami-cf1464a0"
}
variable "stggroup_ins_type" {
  default = "t2.nano"
}
variable "stggroup_pvt_ip" {
  default = "10.0.8.15"
}
variable "stggroup_name" {
  default = "Stage_SOA_Group_API"
}
variable "stggroup_volume_size" {
  default = "30"
}
variable "stggroup_elb_name" {
  default = "stggroup"
}

/*---------------------------------*/

####  Stage Group DB Instance
variable "stggroupdb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stggroupdb_ins_type" {
  default = "t2.nano"
}
variable "stggroupdb_pvt_ip" {
  default = "10.0.9.15"
}
variable "stggroupdb_name" {
  default = "Stage_SOA_Group_DB"
}
variable "stggroupdb_volume_size" {
  default = "30"
}

/*---------------------------------*/

####  Stage School Instance
variable "stgschool_ami_id" {
  default = "ami-8e1666e1"
}
variable "stgschool_ins_type" {
  default = "t2.nano"
}
variable "stgschool_pvt_ip" {
  default = "10.0.8.16"
}
variable "stgschool_name" {
  default = "Stage_SOA_School_API"
}
variable "stgschool_volume_size" {
  default = "30"
}
variable "stgschool_elb_name" {
  default = "stgschool"
}

/*---------------------------------*/

####  Stage School DB Instance
variable "stgschooldb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stgschooldb_ins_type" {
  default = "t2.nano"
}
variable "stgschooldb_pvt_ip" {
  default = "10.0.9.16"
}
variable "stgschooldb_name" {
  default = "Stage_SOA_School_DB"
}
variable "stgschooldb_volume_size" {
  default = "30"
}

/*---------------------------------*/

####  Stage Ums Instance
variable "stgums_ami_id" {
  default = "ami-0e136361"
}
variable "stgums_ins_type" {
  default = "t2.nano"
}
variable "stgums_pvt_ip" {
  default = "10.0.8.17"
}
variable "stgums_name" {
  default = "Stage_SOA_Ums_API"
}
variable "stgums_volume_size" {
  default = "30"
}
variable "stgums_elb_name" {
  default = "stgums"
}

/*---------------------------------*/

####  Stage User DB Instance
variable "stgumsdb_ami_id" {
  default = "ami-61c5b40e"
}
variable "stgumsdb_ins_type" {
  default = "t2.nano"
}
variable "stgumsdb_pvt_ip" {
  default = "10.0.9.17"
}
variable "stgumsdb_name" {
  default = "Stage_SOA_Ums_DB"
}
variable "stgumsdb_volume_size" {
  default = "30"
}

/*---------------------------------*/
