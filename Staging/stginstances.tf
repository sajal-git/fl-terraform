/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance BL
module "stg_soa_bl" {
  source 	  = "github.com/sajal-git/terraform-modules//instance"
  ami_id	  = "${var.stgbl_ami_id}"
  subnet_id	  = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type	  = "${var.stgbl_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.stgbl_pvt_ip}"
  vol_size	  = "${var.stgbl_volume_size}"
  name		  = "${var.stgbl_name}"
}

####  Stage BL Elastic IP
/* module "stg_soa_bl_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_bl.insid}"
}*/

####  Stage BL ELB
module "stg_soa_bl_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stgbl_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_bl.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance BL DB
module "stg_soa_bldb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgbldb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgbldb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgbldb_pvt_ip}"
  vol_size    = "${var.stgbldb_volume_size}"
  name      = "${var.stgbldb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance Admin
module "stg_soa_admin" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgadmin_ami_id}"
  subnet_id   = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgadmin_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgadmin_pvt_ip}"
  vol_size    = "${var.stgadmin_volume_size}"
  name      = "${var.stgadmin_name}"
}

####  Stage Admin Elastic IP
/*module "stg_soa_admin_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_admin.insid}"
}*/

####  Stage Admin ELB
module "stg_soa_admin_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stgadmin_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_admin.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance Admin DB
module "stg_soa_admindb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgadmindb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgadmindb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgadmindb_pvt_ip}"
  vol_size    = "${var.stgadmindb_volume_size}"
  name      = "${var.stgadmindb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance Assets
module "stg_soa_assets" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgassets_ami_id}"
  subnet_id   = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgassets_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgassets_pvt_ip}"
  vol_size    = "${var.stgassets_volume_size}"
  name      = "${var.stgassets_name}"
}

####  Stage Assets Elastic IP
/*module "stg_soa_assets_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_assets.insid}"
}*/

####  Stage Assets ELB
module "stg_soa_assets_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stgassets_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_assets.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance Assets DB
module "stg_soa_assetsdb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgassetsdb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgassetsdb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgassetsdb_pvt_ip}"
  vol_size    = "${var.stgassetsdb_volume_size}"
  name      = "${var.stgassetsdb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance ContentApi
module "stg_soa_content" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgcontent_ami_id}"
  subnet_id   = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgcontent_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgcontent_pvt_ip}"
  vol_size    = "${var.stgcontent_volume_size}"
  name      = "${var.stgcontent_name}"
}

####  Stage Content Elastic IP
/*module "stg_soa_content_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_content.insid}"
}*/

####  Stage Content ELB
module "stg_soa_content_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stgcontent_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_content.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance Content DB
module "stg_soa_contentdb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgcontentdb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgcontentdb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgcontentdb_pvt_ip}"
  vol_size    = "${var.stgcontentdb_volume_size}"
  name      = "${var.stgcontentdb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance Group
module "stg_soa_group" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stggroup_ami_id}"
  subnet_id   = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stggroup_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stggroup_pvt_ip}"
  vol_size    = "${var.stggroup_volume_size}"
  name      = "${var.stggroup_name}"
}

####  Stage Group Elastic IP
/*module "stg_soa_group_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_group.insid}"
}*/

####  Stage Group ELB
module "stg_soa_group_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stggroup_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_group.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance Group DB
module "stg_soa_groupdb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stggroupdb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stggroupdb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stggroupdb_pvt_ip}"
  vol_size    = "${var.stggroupdb_volume_size}"
  name      = "${var.stggroupdb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance School
module "stg_soa_school" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgschool_ami_id}"
  subnet_id   = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgschool_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgschool_pvt_ip}"
  vol_size    = "${var.stgschool_volume_size}"
  name      = "${var.stgschool_name}"
}

####  Stage School Elastic IP
/*module "stg_soa_school_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_school.insid}"
}*/

####  Stage School ELB
module "stg_soa_school_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stgschool_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_school.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance School DB
module "stg_soa_schooldb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgschooldb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgschooldb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgschooldb_pvt_ip}"
  vol_size    = "${var.stgschooldb_volume_size}"
  name      = "${var.stgschooldb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Stage Instance User
module "stg_soa_ums" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgums_ami_id}"
  subnet_id   = "${module.stgapp_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgums_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgums_pvt_ip}"
  vol_size    = "${var.stgums_volume_size}"
  name      = "${var.stgums_name}"
}

####  Stage User Elastic IP
/*module "stg_soa_user_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.stg_soa_ums.insid}"
}*/

####  Stage User ELB
module "stg_soa_ums_elb" {
  source	  = "github.com/sajal-git/terraform-modules//elb/elb_web"
  elb_name	  = "${var.stgums_elb_name}"
  subnet_ids	  = ["${module.public_subnet_01.subnet_id}"]
  instance_ids	  = ["${module.stg_soa_ums.insid}"]
  elb_sg	  = ["${module.elbweb_security_group.elbweb_sgid}"]	
}

####  Stage Instance User DB
module "stg_soa_umsdb" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.stgumsdb_ami_id}"
  subnet_id   = "${module.stgdb_subnet.subnet_id}"
  key_name        = "${module.stg_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.stgumsdb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.stgumsdb_pvt_ip}"
  vol_size    = "${var.stgumsdb_volume_size}"
  name      = "${var.stgumsdb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/
