/*--------------------------------------------------------------------------------------------------------*/

####  Dev SOA API Instance
module "dev_soa_api" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.devsoaapi_ami_id}"
  subnet_id   = "${module.devapp_subnet.subnet_id}"
  key_name        = "${module.dev_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.devsoaapi_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.devsoaapi_pvt_ip}"
  vol_size    = "${var.devsoaapi_volume_size}"
  name      = "${var.devsoaapi_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Dev SOA API Elastic IP
module "dev_soa_api_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.dev_soa_api.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Dev SOA DB Instance 
module "dev_soa_db" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.devsoadb_ami_id}"
  subnet_id   = "${module.devdb_subnet.subnet_id}"
  key_name        = "${module.dev_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.devsoadb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.devsoadb_pvt_ip}"
  vol_size    = "${var.devsoadb_volume_size}"
  name      = "${var.devsoadb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Dev SOA DB Elastic IP
module "dev_soa_db_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.dev_soa_api.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/
