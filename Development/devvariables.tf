####  Key Pair
variable "dev_key_path" {
  default = "keys/dev-key.pub"
} 
variable "devkeyname"{
  default = "dev-key"
} 

####  Dev SOA Api Instances
variable "devsoaapi_ami_id" {
  default = "ami-a60070c9"
}
variable "devsoaapi_ins_type" {
  default = "t2.micro"
}
variable "devsoaapi_pvt_ip" {
  default = "10.0.12.11"
}
variable "devsoaapi_name" {
  default = "Dev_SOA_API_Server"
}
variable "devsoaapi_volume_size" {
  default = "30"
}

####  Dev SOA DB Instance
variable "devsoadb_ami_id" {
  default = "ami-713e4e1e"
}
variable "devsoadb_ins_type" {
  default = "t2.nano"
}
variable "devsoadb_pvt_ip" {
  default = "10.0.13.11"
}
variable "devsoadb_name" {
  default = "Dev_SOA_DB_Server"
}
variable "devsoadb_volume_size" {
  default = "30"
}

