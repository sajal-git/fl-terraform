####  SSH Security Group
module "ssh_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/ssh_sg"
  name            = "Dev Ssh SG"
  source_cidr     = "10.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev SSH Security Group"
}

####  Monitoring Security Group
module "monitoring_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/monitoring_sg"
  name            = "Dev Monitoring SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Monitoring Security Group"
}

####  Web Security Group
module "web_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/web_sg"
  name            = "Dev Web SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Web Security Group"
}

####  Tomcat Security Group
module "tomcat_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/tomcat_sg"
  name            = "Dev Tomcat SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Tomcat Security Group"
}

####  Mysql Security Group
module "mysql_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mysql_sg"
  name            = "Dev Mysql SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Mysql Security Group"
}

####  Mongo Security Group
module "mongo_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mongo_sg"
  name            = "Dev Mongo SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Mongo Security Group"
}

####  Mongoose Security Group
module "mongoose_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mongooseim_sg"
  name            = "Dev Mongoose SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Mongoose Security Group"
}

####  Tungsten Security Group
module "tungsten_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/tungsten_sg"
  name            = "Dev Tungsten SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Dev Tungsten Security Group"
}


####  Key Pair
module "dev_key_pair" {
  source          = "github.com/sajal-git/terraform-modules//key_pair"
  public_key_path = "${var.dev_key_path}"
  keyname         = "${var.devkeyname}"
}
