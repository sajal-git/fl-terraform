/*--------------------------------------------------------*/

####  AWS Keys
variable "aws_access_key" {
  description = "AWS access key."
}
variable "aws_secret_key" {
  description = "AWS secret key."
}

/*--------------------------------------------------------*/

####  AWS Region
variable "aws_region" {
  default = "ap-south-1"
}

/*--------------------------------------------------------*/

####  VPC CIDR
variable "vpc_cidr" {
  description = "Cidr Range for VPC"
  default = "10.0.0.0/16"
}
variable "vpc_tagname" {
  description = "Tag Name For VPC"
  default = "Test VPC"
}

/*--------------------------------------------------------*/

####  Internet Gateway
variable "igw_name" {
  default = "VPC Internat Gateway"
}

/*--------------------------------------------------------*/

####  Public Route Table
variable "public_route_table_cidr" {
  default = "0.0.0.0/0"
}
variable "public_route_table_tagname" {
  default = "Public Route Table"
}

/*--------------------------------------------------------*/

####  Private Route Table
variable "private_route_table_cidr" {
  default = "0.0.0.0/0"
}
variable "private_route_table_tagname" {
  default = "Private Route Table"
}

/*--------------------------------------------------------*/

####  Public Subnet 01
variable "pubsubnet01_cidr" {
  description = "Cidr Range for Public Subnet 01"
  default = "10.0.1.0/24"
}
variable "pubsubnet01_az" {
  default = "ap-south-1a"
}
variable "pubsubnet01_name" {
  default = "Public Subnet 01"
}

/*--------------------------------------------------------*/

####  Public Subnet 02
variable "pubsubnet02_cidr" {
  description = "Cidr Range for Public Subnet 02"
  default = "10.0.2.0/24"
}
variable "pubsubnet02_az" {
  default = "ap-south-1b"
}
variable "pubsubnet02_name" {
  default = "Public Subnet 02"
}

/*--------------------------------------------------------*/

####  Maintenance Subnet
variable "maintenance_cidr" {
  description = "Cidr Range for Maintenance Subnet"
  default = "10.0.3.0/24"
}
variable "maintenance_az" {
  default = "ap-south-1a"
}
variable "maintenance_name" {
  default = "Maintenance Subnet"
}

/*--------------------------------------------------------*/

####  Production App Subnet 01
variable "prodapp01_cidr" {
  description = "Cidr Range for Production App Subnet 01"
  default = "10.0.4.0/24"
}
variable "prodapp01_az" {
  default = "ap-south-1a"
}
variable "prodapp01_name" {
  default = "Production App Subnet 01"
}

/*--------------------------------------------------------*/

####  Production App Subnet 02
variable "prodapp02_cidr" {
  description = "Cidr Range for Production App Subnet 02"
  default = "10.0.5.0/24"
}
variable "prodapp02_az" {
  default = "ap-south-1b"
}
variable "prodapp02_name" {
  default = "Production App Subnet 02"
}

/*--------------------------------------------------------*/

####  Production DB Subnet 01
variable "proddb01_cidr" {
  description = "Cidr Range for Production DB Subnet 01"
  default = "10.0.6.0/24"
}
variable "proddb01_az" {
  default = "ap-south-1a"
}
variable "proddb01_name" {
  default = "Production DB Subnet 01"
}

/*--------------------------------------------------------*/

####  Production DB Subnet 02
variable "proddb02_cidr" {
  description = "Cidr Range for Production DB Subnet 02"
  default = "10.0.7.0/24"
}
variable "proddb02_az" {
  default = "ap-south-1b"
}
variable "proddb02_name" {
  default = "Production DB Subnet 02"
}

/*--------------------------------------------------------*/

####  Staging App Subnet
variable "stgapp_cidr" {
  description = "Cidr Range for Staging App Subnet"
  default = "10.0.8.0/24"
}
variable "stgapp_az" {
  default = "ap-south-1a"
}
variable "stgapp_name" {
  default = "Staging App Subnet"
}

/*--------------------------------------------------------*/

####  Staging DB Subnet
variable "stgdb_cidr" {
  description = "Cidr Range for Staging DB Subnet"
  default = "10.0.9.0/24"
}
variable "stgdb_az" {
  default = "ap-south-1a"
}
variable "stgdb_name" {
  default = "Staging DB Subnet"
}

/*--------------------------------------------------------*/

####  Integration App Subnet
variable "intapp_cidr" {
  description = "Cidr Range for Integration App Subnet"
  default = "10.0.10.0/24"
}
variable "intapp_az" {
  default = "ap-south-1a"
}
variable "intapp_name" {
  default = "Integration App Subnet"
}

/*--------------------------------------------------------*/

####  Integration DB Subnet
variable "intdb_cidr" {
  description = "Cidr Range for Integration DB Subnet"
  default = "10.0.11.0/24"
}
variable "intdb_az" {
  default = "ap-south-1a"
}
variable "intdb_name" {
  default = "Integration DB Subnet"
}

/*--------------------------------------------------------*/

####  Development App Subnet
variable "devapp_cidr" {
  description = "Cidr Range for Development App Subnet"
  default = "10.0.12.0/24"
}
variable "devapp_az" {
  default = "ap-south-1a"
}
variable "devapp_name" {
  default = "Development App Subnet"
}

/*--------------------------------------------------------*/

####  Development DB Subnet
variable "devdb_cidr" {
  description = "Cidr Range for Development DB Subnet"
  default = "10.0.13.0/24"
}
variable "devdb_az" {
  default = "ap-south-1a"
}
variable "devdb_name" {
  default = "Development DB Subnet"
}

/*--------------------------------------------------------*/

####  Testing Public Subnet 01
variable "testpub1_cidr" {
  description = "Cidr Range for Testing Public Subnet 01"
  default = "10.0.14.0/24"
}
variable "testpub1_az" {
  default = "ap-south-1a"
}
variable "testpub1_name" {
  default = "Testing Public Subnet 01"
}

/*--------------------------------------------------------*/

####  Testing Public Subnet 02
variable "testpub2_cidr" {
  description = "Cidr Range for Testing Public Subnet 02"
  default = "10.0.15.0/24"
}
variable "testpub2_az" {
  default = "ap-south-1b"
}
variable "testpub2_name" {
  default = "Testing Public Subnet 02"
}

/*-------------------------------------------------------*/

####  Testing Private Subnet 01
variable "testpvt1_cidr" {
  description = "Cidr Range for Testing Private Subnet 01"
  default = "10.0.16.0/24"
}
variable "testpvt1_az" {
  default = "ap-south-1a"
}
variable "testpvt1_name" {
  default = "Testing Private Subnet 01"
}

/*--------------------------------------------------------*/

####  Testing Private Subnet 02
variable "testpvt2_cidr" {
  description = "Cidr Range for Testing Private Subnet 02"
  default = "10.0.17.0/24"
}
variable "testpvt2_az" {
  default = "ap-south-1b"
}
variable "testpvt2_name" {
  default = "Testing Private Subnet 02"
}

/*-------------------------------------------------------*/

####  NAT Key Pair 
variable "pub_key_path" {
  default = "keys/nat-key.pub"
}
variable "keyname"{
  default = "nat-key"
}

####  NAT Instances 
variable "nat_ami_id" {
  default = "ami-801666ef"
}
variable "nat_ins_type" {
  default = "t2.micro"
}
variable "nat1_pvt_ip" {
  default = "10.0.1.10"
}
variable "nat1_vol" {
  default = "8"
}
variable "nat1_name" {
  default = "NAT Instance AZ1"
}
variable "nat2_pvt_ip" {
  default = "10.0.2.10"
}
variable "nat2_vol" {
  default = "8"
}
variable "nat2_name" {
  default = "NAT Instance AZ2"
}

/*--------------------------------------------------------*/


