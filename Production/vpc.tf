/*------------------------------------------------------------------------------*/

####  Provider Info 
provider "aws"{
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.aws_region}"
}

/*-----------------------------------------------------------------------------*/

####  VPC 
module "vpc" {
  source = "github.com/sajal-git/terraform-modules//vpc"
  vpc_cidr = "${var.vpc_cidr}"
  vpc_name = "${var.vpc_tagname}"
}

/*-----------------------------------------------------------------------------*/

####  IGW 
module "igw" {        
  source = "github.com/sajal-git/terraform-modules//igw"
  vpcid = "${module.vpc.vpcid}"
  igw_name = "${var.igw_name}"
}

/*-----------------------------------------------------------------------------*/

####  Public Route Table 
module "public_route_table" {
  source = "github.com/sajal-git/terraform-modules//route_table"
  vpcid = "${module.vpc.vpcid}"
  cidr_block = "${var.public_route_table_cidr}"
  igwid = "${module.igw.igwid}"
  rt_name = "${var.public_route_table_tagname}"
}

/*-----------------------------------------------------------------------------*/

####  Private Route Table 
module "private_route_table" {
  source = "github.com/sajal-git/terraform-modules//route_table"
  vpcid = "${module.vpc.vpcid}"
  cidr_block = "${var.private_route_table_cidr}"
  natinstance = "${module.nat1_ins.insid}"
  rt_name = "${var.private_route_table_tagname}"
}

/*-----------------------------------------------------------------------------*/

####  Public Subnet 01 
module "public_subnet_01" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.pubsubnet01_cidr}"
  az = "${var.pubsubnet01_az}"
  subnet_name = "${var.pubsubnet01_name}"
}


####  Public Subnet 01 RT Association
module "public_subnet_01_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.public_subnet_01.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Public Subnet 02 
module "public_subnet_02" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.pubsubnet02_cidr}"
  az = "${var.pubsubnet02_az}"
  subnet_name = "${var.pubsubnet02_name}"
}


####  Public Subnet 02 RT Association
module "public_subnet_02_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.public_subnet_02.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Maintenance Subnet
module "maintenance_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.maintenance_cidr}"
  az = "${var.maintenance_az}"
  subnet_name = "${var.maintenance_name}"
}


####  Maintenance RT Association
module "maintenance_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.maintenance_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Production App Subnet 01 
module "prodapp_subnet_01" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.prodapp01_cidr}"
  az = "${var.prodapp01_az}"
  subnet_name = "${var.prodapp01_name}"
}


####  Production App Subnet 01 RT Association
module "prodapp_subnet_01_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.prodapp_subnet_01.subnet_id}"
  rt_id = "${module.private_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Production App Subnet 02 
module "prodapp_subnet_02" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.prodapp02_cidr}"
  az = "${var.prodapp02_az}"
  subnet_name = "${var.prodapp02_name}"
}


####  Production App Subnet 02 RT Association
module "prodapp_subnet_02_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.prodapp_subnet_02.subnet_id}"
  rt_id = "${module.private_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Production DB Subnet 01 
module "proddb_subnet_01" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.proddb01_cidr}"
  az = "${var.proddb01_az}"
  subnet_name = "${var.proddb01_name}"
}


####  Production DB Subnet 01 RT Association
module "proddb_subnet_01_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.proddb_subnet_01.subnet_id}"
  rt_id = "${module.private_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Production DB Subnet 02 
module "proddb_subnet_02" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.proddb02_cidr}"
  az = "${var.proddb02_az}"
  subnet_name = "${var.proddb02_name}"
}


####  Production DB Subnet 02 RT Association
module "proddb_subnet_02_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.proddb_subnet_02.subnet_id}"
  rt_id = "${module.private_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Staging App Subnet
module "stgapp_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.stgapp_cidr}"
  az = "${var.stgapp_az}"
  subnet_name = "${var.stgapp_name}"
}


####  Staging App Subnet RT Association
module "stgapp_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.stgapp_subnet.subnet_id}"
  rt_id = "${module.private_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Staging DB Subnet
module "stgdb_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.stgdb_cidr}"
  az = "${var.stgdb_az}"
  subnet_name = "${var.stgdb_name}"
}


####  Staging DB Subnet RT Association
module "stgdb_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.stgdb_subnet.subnet_id}"
  rt_id = "${module.private_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Integration App Subnet
module "intapp_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.intapp_cidr}"
  az = "${var.intapp_az}"
  subnet_name = "${var.intapp_name}"
}


####  Integration App Subnet RT Association
module "intapp_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.intapp_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Integration DB Subnet
module "intdb_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.intdb_cidr}"
  az = "${var.intdb_az}"
  subnet_name = "${var.intdb_name}"
}


####  Integration DB Subnet RT Association
module "intdb_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.intdb_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Development App Subnet
module "devapp_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.devapp_cidr}"
  az = "${var.devapp_az}"
  subnet_name = "${var.devapp_name}"
}


####  Development App Subnet RT Association
module "devapp_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.devapp_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Development DB Subnet
module "devdb_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.devdb_cidr}"
  az = "${var.devdb_az}"
  subnet_name = "${var.devdb_name}"
}


####  Development DB Subnet RT Association
module "devdb_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.devdb_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Testing Public Subnet 01
module "testpub1_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.testpub1_cidr}"
  az = "${var.testpub1_az}"
  subnet_name = "${var.testpub1_name}"
}


####  Testing Public Subnet 01 RT Association
module "testpub1_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.testpub1_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Testing Public Subnet 02
module "testpub2_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.testpub2_cidr}"
  az = "${var.testpub2_az}"
  subnet_name = "${var.testpub2_name}"
}


####  Testing Public Subnet 02 RT Association
module "testpub2_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.testpub2_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Testing Private Subnet 01
module "testpvt1_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.testpvt1_cidr}"
  az = "${var.testpvt1_az}"
  subnet_name = "${var.testpvt1_name}"
}


####  Testing Private Subnet 01 RT Association
module "testpvt1_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.testpvt1_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  Testing Private Subnet 02
module "testpvt2_subnet" {
  source = "github.com/sajal-git/terraform-modules//subnet"
  vpcid = "${module.vpc.vpcid}"
  cidr = "${var.testpvt2_cidr}"
  az = "${var.testpvt2_az}"
  subnet_name = "${var.testpvt2_name}"
}


####  Testing Private Subnet 02 RT Association
module "testpvt2_subnet_association" {
  source = "github.com/sajal-git/terraform-modules//route_table_association"
  subnet_id = "${module.testpvt2_subnet.subnet_id}"
  rt_id = "${module.public_route_table.rtid}"
}

/*-----------------------------------------------------------------------------*/

####  NAT Security Group  
module "nat_security_group" {
  source 	  = "github.com/sajal-git/terraform-modules//sec_grp/nat_sg"
  name 		  = "NAT_SG"
  source_cidr	  = "0.0.0.0/0"
  vpcid 	  = "${module.vpc.vpcid}"
  tagname 	  = "NAT Security Group"
}


####  Key Pair  
module "key_pair" {
  source          = "github.com/sajal-git/terraform-modules//key_pair"
  public_key_path = "${var.pub_key_path}"
  keyname         = "${var.keyname}"
}


####  NAT Instance 1  
module "nat1_ins" {
  source 	  = "github.com/sajal-git/terraform-modules//nat_instance"
  ami_id	  = "${var.nat_ami_id}"
  subnet_id	  = "${module.public_subnet_01.subnet_id}"
  key_name        = "${module.key_pair.key_name}"
  vpc_security_groups = ["${module.nat_security_group.nat_sgid}"]
  ins_type	  = "${var.nat_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.nat1_pvt_ip}"
  vol_size	  = "${var.nat1_vol}"
  name		  = "${var.nat1_name}"
}


####  NAT Instance 2  
module "nat2_ins" {
  source 	  = "github.com/sajal-git/terraform-modules//nat_instance"
  ami_id	  = "${var.nat_ami_id}"
  subnet_id	  = "${module.public_subnet_02.subnet_id}"
  key_name        = "${module.key_pair.key_name}"
  vpc_security_groups = ["${module.nat_security_group.nat_sgid}"]
  ins_type	  = "${var.nat_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.nat2_pvt_ip}"
  vol_size	  = "${var.nat2_vol}"
  name		  = "${var.nat2_name}"
}


####  Elastic IP  
module "nat1_eip" {
  source 	  = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id	  = "${module.nat1_ins.insid}"
}
module "nat2_eip" {
  source 	  = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id	  = "${module.nat2_ins.insid}"
}

/*-----------------------------------------------------------------------------*/
