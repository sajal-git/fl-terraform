/*--------------------------------------------------------*/

####  Key Pair
variable "int_key_path" {
  default = "keys/int-key.pub"
} 
variable "intkeyname"{
  default = "int-key"
} 

/*--------------------------------------------------------*/

####  Int SOA Api Instances
variable "intmigsoaapi_ami_id" {
  default = "ami-611d6d0e"
}
variable "intmigsoaapi_ins_type" {
  default = "t2.micro"
}
variable "intmigsoaapi_pvt_ip" {
  default = "10.0.10.11"
}
variable "intmigsoaapi_name" {
  default = "Intmig_SOA_API_Server"
}
variable "intmigsoaapi_volume_size" {
  default = "30"
}

/*--------------------------------------------------------*/

####  Int SOA DB Instance
variable "intmigsoadb_ami_id" {
  default = "ami-61c5b40e"
}
variable "intmigsoadb_ins_type" {
  default = "t2.micro"
}
variable "intmigsoadb_pvt_ip" {
  default = "10.0.11.11"
}
variable "intmigsoadb_name" {
  default = "Intmig_SOA_DB_Server"
}
variable "intmigsoadb_volume_size" {
  default = "250"
}

/*--------------------------------------------------------*/

####  Int DB Migration OLD DB Instance
variable "intmigolddb_ami_id" {
  default = "ami-61c5b40e"
}
variable "intmigolddb_ins_type" {
  default = "t2.micro"
}
variable "intmigolddb_pvt_ip" {
  default = "10.0.11.12"
}
variable "intmigolddb_name" {
  default = "Intmig_OLD_DB_Server"
}
variable "intmigolddb_volume_size" {
  default = "250"
}

/*--------------------------------------------------------*/
