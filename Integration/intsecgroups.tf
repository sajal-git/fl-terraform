/*-----------------------------------------------------------------------------------------*/

####  SSH Security Group
module "ssh_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/ssh_sg"
  name            = "Int Ssh SG"
  source_cidr     = "10.0.0.0/16"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int SSH Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Monitoring Security Group
module "monitoring_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/monitoring_sg"
  name            = "Int Monitoring SG"
  source_cidr     = "10.0.3.0/24"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Monitoring Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Web Security Group
module "web_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/web_sg"
  name            = "Int Web SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Web Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Tomcat Security Group
module "tomcat_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/tomcat_sg"
  name            = "Int Tomcat SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Tomcat Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Mysql Security Group
module "mysql_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mysql_sg"
  name            = "Int Mysql SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Mysql Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Mongo Security Group
module "mongo_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mongo_sg"
  name            = "Int Mongo SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Mongo Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Mongoose Security Group
module "mongoose_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/mongooseim_sg"
  name            = "Int Mongoose SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Mongoose Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Tungsten Security Group
module "tungsten_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/tungsten_sg"
  name            = "Int Tungsten SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Int Tungsten Security Group"
}

/*-----------------------------------------------------------------------------------------*/

####  Key Pair
module "int_key_pair" {
  source          = "github.com/sajal-git/terraform-modules//key_pair"
  public_key_path = "${var.int_key_path}"
  keyname         = "${var.intkeyname}"
}

/*-----------------------------------------------------------------------------------------*/
