/*--------------------------------------------------------------------------------------------------------*/

####  Int DB Migration SOA Instance
module "intmig_soa_api" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.intmigsoaapi_ami_id}"
  subnet_id   = "${module.intapp_subnet.subnet_id}"
  key_name        = "${module.int_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.intmigsoaapi_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.intmigsoaapi_pvt_ip}"
  vol_size    = "${var.intmigsoaapi_volume_size}"
  name      = "${var.intmigsoaapi_name}"
}

####  Int DB Migration SOA Elastic IP
module "intmig_soa_api_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.intmig_soa_api.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Int DB Migration SOA DB 
module "intmig_soa_db" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.intmigsoadb_ami_id}"
  subnet_id   = "${module.intdb_subnet.subnet_id}"
  key_name        = "${module.int_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.intmigsoadb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.intmigsoadb_pvt_ip}"
  vol_size    = "${var.intmigsoadb_volume_size}"
  name      = "${var.intmigsoadb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Int DB Migration OLD DB Instance
module "intmig_old_db" {
  source    = "github.com/sajal-git/terraform-modules//instance"
  ami_id    = "${var.intmigolddb_ami_id}"
  subnet_id   = "${module.intdb_subnet.subnet_id}"
  key_name        = "${module.int_key_pair.key_name}"
  vpc_security_groups = ["${module.mysql_security_group.mysql_sgid}","${module.ssh_security_group.ssh_sgid}"]
  ins_type    = "${var.intmigolddb_ins_type}"
  pub_ip    = "true"
  pvt_ip    = "${var.intmigolddb_pvt_ip}"
  vol_size    = "${var.intmigolddb_volume_size}"
  name      = "${var.intmigolddb_name}"
}

/*--------------------------------------------------------------------------------------------------------*/
