/*--------------------------------------------------------------------------------------------------------*/

####  Jenkins Instance
module "jenkins_ins" {
  source 	  = "github.com/sajal-git/terraform-modules//instance"
  ami_id	  = "${var.jenkins_ami_id}"
  subnet_id	  = "${module.maintenance_subnet.subnet_id}"
  key_name        = "${module.mnt_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}","${module.jenkins_security_group.jenkins_sgid}","${module.monitoring_security_group.mon_sgid}"]
  ins_type	  = "${var.jenkins_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.jenkins_pvt_ip}"
  vol_size	  = "${var.jenkins_vol}"
  name		  = "${var.jenkins_name}"
}

####  Jenkins Elastic IP
module "jenkins_ins_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.jenkins_ins.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  Ldap Instance

module "ldap_ins" {
  source 	  = "github.com/sajal-git/terraform-modules//instance"
  ami_id	  = "${var.ldap_ami_id}"
  subnet_id	  = "${module.maintenance_subnet.subnet_id}"
  key_name        = "${module.mnt_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}","${module.ldap_security_group.ldap_sgid}"]
  ins_type	  = "${var.ldap_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.ldap_pvt_ip}"
  vol_size	  = "${var.ldap_vol}"
  name		  = "${var.ldap_name}"
}

####  Ldap Elastic IP
module "ldap_ins_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.ldap_ins.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  VPN Instance

module "vpn_ins" {
  source 	  = "github.com/sajal-git/terraform-modules//instance"
  ami_id	  = "${var.vpn_ami_id}"
  subnet_id	  = "${module.maintenance_subnet.subnet_id}"
  key_name        = "${module.mnt_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}","${module.vpn_security_group.vpn_sgid}"]
  ins_type	  = "${var.vpn_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.vpn_pvt_ip}"
  vol_size	  = "${var.vpn_vol}"
  name		  = "${var.vpn_name}"
}

####  VPN Elastic IP
module "vpn_ins_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.vpn_ins.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/

####  OMD Instance

module "omd_ins" {
  source 	  = "github.com/sajal-git/terraform-modules//instance"
  ami_id	  = "${var.omd_ami_id}"
  subnet_id	  = "${module.maintenance_subnet.subnet_id}"
  key_name        = "${module.mnt_key_pair.key_name}"
  vpc_security_groups = ["${module.web_security_group.web_sgid}","${module.ssh_security_group.ssh_sgid}","${module.omd_security_group.omd_sgid}"]
  ins_type	  = "${var.omd_ins_type}"
  pub_ip	  = "true"
  pvt_ip	  = "${var.omd_pvt_ip}"
  vol_size	  = "${var.omd_vol}"
  name		  = "${var.omd_name}"
}

####  OMD Elastic IP
module "omd_ins_eip" {
  source          = "github.com/sajal-git/terraform-modules//elasticip"
  ins_id          = "${module.omd_ins.insid}"
}

/*--------------------------------------------------------------------------------------------------------*/