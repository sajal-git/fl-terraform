/*--------------------------------------------------------*/

####  Key Pair
variable "mnt_key_path" {
  default = "keys/devops-key.pub"
} 
variable "mntkeyname"{
  default = "devops-key"
} 

/*--------------------------------------------------------*/

####  Jenkins Instance
variable "jenkins_ami_id" {
  default = "ami-fa166695"
}
variable "jenkins_ins_type" {
  default = "t2.micro"
}
variable "jenkins_pvt_ip" {
  default = "10.0.3.11"
}
variable "jenkins_name" {
  default = "Jenkins Server"
}
variable "jenkins_vol" {
  default = "50"
}

/*--------------------------------------------------------*/

####  Ldap Instance
variable "ldap_ami_id" {
  default = "ami-cac3b2a5"
}
variable "ldap_ins_type" {
  default = "t2.nano"
}
variable "ldap_pvt_ip" {
  default = "10.0.3.12"
}
variable "ldap_name" {
  default = "Ldap Server"
}
variable "ldap_vol" {
  default = "20"
}

/*--------------------------------------------------------*/

####   VPN Instance
variable "vpn_ami_id" {
  default = "ami-b92f5fd6"
}
variable "vpn_ins_type" {
  default = "t2.micro"
}
variable "vpn_pvt_ip" {
  default = "10.0.3.13"
}
variable "vpn_name" {
  default = "VPN Server"
}
variable "vpn_vol" {
  default = "20"
}

/*--------------------------------------------------------*/

####   OMD Instance
variable "omd_ami_id" {
  default = "ami-e92e5e86"
}
variable "omd_ins_type" {
  default = "t2.micro"
}
variable "omd_pvt_ip" {
  default = "10.0.3.14"
}
variable "omd_name" {
  default = "OMD Server"
}
variable "omd_vol" {
  default = "20"
}

/*--------------------------------------------------------*/