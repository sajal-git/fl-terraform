####  SSH Security Group
module "ssh_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/ssh_sg"
  name            = "Mnt Ssh SG"
  source_cidr     = "10.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt SSH Security Group"
}

####  Monitoring Security Group
module "monitoring_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/monitoring_sg"
  name            = "Mnt Monitoring SG"
  source_cidr     = "10.0.3.14/32"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt Monitoring Security Group"
}

####  Web Security Group
module "web_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/web_sg"
  name            = "Mnt Web SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt Web Security Group"
}

####  Chef Security Group
module "chef_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/chef_sg"
  name            = "Mnt Chef SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt Chef Security Group"
}

####  Jenkins Security Group
module "jenkins_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/jenkins_sg"
  name            = "Mnt Jenkins SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt Jenkins Security Group"
}

####  OMD Security Group
module "omd_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/omd_sg"
  name            = "Mnt Omd SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt OMD Security Group"
}

####  Ldap Security Group
module "ldap_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/ldap_sg"
  name            = "Mnt Ldap SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt Ldap Security Group"
}

####  Vpn Security Group
module "vpn_security_group" {
  source          = "github.com/sajal-git/terraform-modules//sec_grp/vpn_sg"
  name            = "Mnt Vpn SG"
  source_cidr     = "0.0.0.0/0"
  vpcid           = "${module.vpc.vpcid}"
  tagname         = "Mnt Vpn Security Group"
}


####  Key Pair
module "mnt_key_pair" {
  source          = "github.com/sajal-git/terraform-modules//key_pair"
  public_key_path = "${var.mnt_key_path}"
  keyname         = "${var.mntkeyname}"
}
